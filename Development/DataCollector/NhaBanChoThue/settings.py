# -*- coding: utf-8 -*-

# Scrapy settings for NhaBanChoThue project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'SiteSpider'

SPIDER_MODULES = ['NhaBanChoThue.spiders']
NEWSPIDER_MODULE = 'NhaBanChoThue.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'NhaBanChoThue (+http://www.yourdomain.com)'
