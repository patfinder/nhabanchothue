# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class PostItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    code = scrapy.Field()
    title = scrapy.Field()
    desc = scrapy.Field()
    time = scrapy.Field()
    ownerID = scrapy.Field()  # Owner ID
    ownerName = scrapy.Field()  # Owner name
    type = scrapy.Field()
    price = scrapy.Field()
    area = scrapy.Field()
    address = scrapy.Field()
    city = scrapy.Field()
    district = scrapy.Field()
    image = scrapy.Field()
    status = scrapy.Field()
    expire = scrapy.Field()
    views = scrapy.Field()

