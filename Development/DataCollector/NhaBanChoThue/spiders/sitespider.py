# -*- coding: utf-8 -*-
from __builtin__ import staticmethod
import scrapy
import urllib
from scrapy import log
from scrapy.http.request import Request
from scrapy.http.response import Response
from scrapy.http.response.text import TextResponse
import re

from NhaBanChoThue.items import *

class SiteSpider(scrapy.Spider):
    itemCount = 0
    name = "SiteSpider"
    allowed_domains = []

    # start_urls = (
    # 'http://www.thuephongtro.com/',
    # )

    def __init__(self, *args, **kwargs):
        super(SiteSpider, self).__init__(*args, **kwargs)

        self.propKeys = {}
        self.params = {}

        for k, v in kwargs.iteritems():
            if k[:5] == "item_":
                propName = k[5:]
                self.propKeys[propName] = v
            else:
                self.params[k] = v

        page = self.params["page"]

        self.domain = self.params["domain"]
        self.host = ("http://" if page.startswith("http://") else "https://") + self.domain + "/"
        self.dryrun = SiteSpider.strtobool(None if "dryrun" not in self.params else self.params["dryrun"])

    def start_requests(self):

        # Build initial request URL
        url = self.params["page"].replace("##page##", "1")

        return [scrapy.Request(url)]

    @staticmethod
    def strip_str(str1):
        """
        Return striped input string if it is a string, other None
        :param str1: str
        :return: str or None
        """
        return None if not str1 else str1.strip()

    @staticmethod
    def strtobool(str1):
        """
        Return bool value from string: True = 1, true, yes ...
        :param str1: str
        :return:
        """
        return isinstance(str1, str) and \
               str1.strip().lower() in ['true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly', 'uh-huh']

    def parse(self, response):
        """
        :param response: Reponse content
        :type response: TextResponse
        :return:
        """

        # Check if this is a listing page.
        # TODO: Check URL and RegEx conflict (escape RegEx special chars)
        url = self.params["page"].replace("##page##", "(\\d+)")
        pattern = re.compile(url)
        match = pattern.match(response.url)

        newPageList = []

        # =================================================
        # This is a listing page
        if match:

            # =================================================
            # Create page list if this page is 1 or dividable to 10
            page = int(match.group(1))
            if page == 1 or page % 10 == 0:  # Page 11 duplicate is ok
                # Add next 10 pages
                newPageList = [Request(self.params["page"].replace("##page##", str(p)))
                               for p in range(page + 1, page + 11)]

            # =================================================
            # Get item urls of this page; ilink
            linkKey = self.params["ilink"]

            # TODO: Check if URL is relative or not including domain, then normalize
            itemPageUrls = [Request(self.host + u) for u in response.xpath(linkKey).extract()]

            return newPageList + itemPageUrls

        # TODO: debug
        if SiteSpider.itemCount > 10:
            return None

        SiteSpider.itemCount += 1

        # Create post item
        post = PostItem()

        # =================================================
        # This is a detail page
        for propName, propKey in self.propKeys.iteritems():

            elemValue = response.xpath(propKey).extract()
            if not elemValue:
                continue

            post[propName] = SiteSpider.strip_str(elemValue[0])

            # codeKey = None if "item_code" not in self.params else self.params["item_code"]
            # titleKey = None if "item_title" not in self.params else self.params["item_title"]
            # descKey = self.params["item_desc"]
            # timeKey = self.params["item_time"]
            # ownerIDKey = self.params['item_ownerid']
            # ownerNameKey = self.params['item_ownername']
            # typeKey = self.params['item_type']
            # priceKey = self.params['item_price']
            # areaKey = self.params['item_area']
            # addressKey = self.params['item_address']
            # cityKey = self.params['item_city']
            # districtKey = self.params['item_district']
            # imageKey = self.params['item_image']
            # statusKey = self.params['item_status']
            # expireKey = self.params['item_expire']
            # viewsKey = self.params['item_views']

            # post['code'] = SiteSpider.strip_str((response.xpath(codeKey).extract() or [None])[0])

        return post
        # pass