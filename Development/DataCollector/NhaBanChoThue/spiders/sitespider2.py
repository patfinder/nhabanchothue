# -*- coding: utf-8 -*-
import scrapy
import urllib
from scrapy import log
from scrapy.http.request import Request
from scrapy.http.response import Response
from scrapy.http.response.text import TextResponse
import re

from NhaBanChoThue.items import *

class SiteSpider2(scrapy.Spider):
    name = "SiteSpider2"
    allowed_domains = []

    # start_urls = (
    # 'http://www.thuephongtro.com/',
    # )

    def __init__(self, *args, **kwargs):
        super(SiteSpider2, self).__init__(*args, **kwargs)

        self.params = kwargs
        page = self.params["page"]

        self.domain = self.params["domain"]
        self.host = ("http://" if page.startswith("http://") else "https://") + self.domain + "/"

    def start_requests(self):

        # Build initial request URL
        url = self.params["page"].replace("##page##", "1")

        return [scrapy.Request(url)]

    @staticmethod
    def strip_str(str1):
        """
        Return striped input string if it is a string, other None
        :param str1: str
        :return: str or None
        """
        return None if not str1 else str1.strip()

    def parse(self, response):
        """
        :param response: Reponse content
        :type response: TextResponse
        :return:
        """

        # Check if this is a listing page.
        # TODO: Check URL and RegEx conflict (escape RegEx special chars)
        url = self.params["page"].replace("##page##", "(\\d+)")
        pattern = re.compile(url)
        match = pattern.match(response.url)

        newPageList = []

        # =================================================
        # This is a listing page
        if match:

            # =================================================
            # Create page list if this page is 1 or dividable to 10
            page = int(match.group(1))
            if page == 1 or page % 10 == 0:  # Page 11 duplicate is ok
                # Add next 10 pages
                newPageList = [Request(self.params["page"].replace("##page##", str(p)))
                               for p in range(page + 1, page + 11)]

            # =================================================
            # Get item urls of this page; ilink
            linkKey = self.params["ilink"]

            # TODO: Check if URL is relative or not including domain, then normalize
            itemPageUrls = [Request(self.host + u) for u in response.xpath(linkKey).extract()]

            return newPageList + itemPageUrls

        # =================================================
        # This is a detail page
        codeKey = None if "item_code" not in self.params else self.params["item_code"]
        titleKey = None if "item_title" not in self.params else self.params["item_title"]
        # descKey = self.params["item_desc"]
        # timeKey = self.params["item_time"]
        # ownerIDKey = self.params['item_ownerid']
        # ownerNameKey = self.params['item_ownername']
        # typeKey = self.params['item_type']
        # priceKey = self.params['item_price']
        # areaKey = self.params['item_area']
        # addressKey = self.params['item_address']
        # cityKey = self.params['item_city']
        # districtKey = self.params['item_district']
        # imageKey = self.params['item_image']
        # statusKey = self.params['item_status']
        # expireKey = self.params['item_expire']
        # viewsKey = self.params['item_views']

        post = PostItem()
        post['code'] = SiteSpider2.strip_str((response.xpath(codeKey).extract() or [None])[0])

        #return post
        pass